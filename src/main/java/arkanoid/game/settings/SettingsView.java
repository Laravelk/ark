package arkanoid.game.settings;

import javax.swing.*;
import java.awt.*;

public class SettingsView extends JFrame {

    private JSlider volumeSlider = new JSlider(0, 100, 1);
    private JSlider tickrateSlider = new JSlider(15, 60, 15);
    private JButton okButton = new JButton("Ok");

    SettingsView() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(400, 200));

        setLayout(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();

        tickrateSlider.setValue(30);

        JLabel label = new JLabel("Volume: ");
        constraints.weightx = 0;
        constraints.weighty = 0;
        add(label, constraints);

        constraints.weightx = 1;
        constraints.weighty = 0;
        add(volumeSlider, constraints);

        JLabel label2 = new JLabel("Tickrate: ");
        constraints.weightx = 0;
        constraints.weighty = 1;
        add(label2, constraints);

        constraints.weightx = 1;
        constraints.weighty = 1;
        add(tickrateSlider, constraints);

        constraints.weightx = 0;
        constraints.weighty = 2;
        constraints.gridwidth = 2;
        add(okButton, constraints);
    }

    JSlider getVolumeSlider() {
        return volumeSlider;
    }

    JSlider getTickrateSlider() {
        return tickrateSlider;
    }

    JButton getOkButton() {
        return okButton;
    }
}
