package arkanoid.game;

import arkanoid.SoundDriver;
import arkanoid.game.settings.SettingsController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class GameController {
    public GameController(SettingsController settingsController) {
        JFrame frame = new JFrame("Arkanoid");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setSize(1280, 740);
        frame.setLocationRelativeTo(null);
        GameView view = new GameView(1280, 700, settingsController.getTickrate());
        frame.add(view);
        frame.setVisible(true);
        view.setVisible(true);

        view.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE
                        && !view.isRunning()) {
                    view.run();
                };
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    view.togglePause();}
                if (e.getKeyCode() == KeyEvent.VK_Q) {
                    view.quit();
                }
            }
        });


        view.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                view.playerMoved(e.getX() - view.getWidth() / 2);
            }
        });

    }
}
