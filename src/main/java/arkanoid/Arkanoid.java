package arkanoid;

import arkanoid.menuwindow.Controller;

import java.io.File;

public class Arkanoid {
    public static void main(String[] args) {
        String music = "src/main/resources/main_menu.wav";
        SoundDriver soundDriver = new SoundDriver(new File(music));
        soundDriver.play();
        Controller gameMenuController = new Controller(soundDriver);
    }
}
