package arkanoid.menuwindow;

import javax.swing.*;
import java.awt.*;

// main game menu view
class View extends JFrame {
    View() {
        init();
    }

    private final JButton newGameButton = new JButton("New Game");
    private final JButton settingButton = new JButton("Settings");

    // init window
    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(300, 100));
        setResizable(false);
        setTitle("Arkanoid");

        setLayout(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 0.1f;
        constraints.weighty = 0.1f;
        add(newGameButton, constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 1;
        add(settingButton, constraints);
    }

    /*
     * @return create new game button
     * */
    JButton getCreateNewGameButton() {
        return newGameButton;
    }
    /*
    * @return settings button
    * */

    JButton getSettingButton() {
        return settingButton;
    }
}
