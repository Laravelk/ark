package gameobjects;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Block implements GameObject {
    private Point position = new Point(0, 0);
    private int width = 70;
    private int height = 30;
    private Color color = Color.red.darker();

    private static Logger logger = Logger.getLogger(Block.class.getName());

    public Block() {}

    /*
    * width is width of block
    * height is height of block
    * color is color of block
    * */
    public Block(int width, int height, Color color) {
        if (0 > width) {
            throw new IllegalArgumentException("Invalid width: " + width);
        }
        if (0 > height) {
            throw new IllegalArgumentException("Invalid height: " + height);
        }
        if (null == color) {
            throw new IllegalArgumentException("Invalid color");
        }
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public Block(Point position, final int width, final int height, final Color color) {
        if (0 > width || 0 > height || null == position || null == color) throw new AssertionError();
        this.position = position;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    /*
    * calculate new ball point by hit box
    * @return new current point
    * */
    public Point bounceVector(Rectangle box) {
        assert null != box;
        logger.log(Level.INFO, "Calculate a bounce vector for hit box: {0}", box);
        final int height_const = 3;
        final int width_const = 10;
        final int xConst = 1;
        final int yConst = 1;
        final int xDefault = -1;
        final int yDefault = -1;
        Point p = new Point(xConst, yConst);
        Rectangle hbT = new Rectangle(position.x, position.y, width, height / height_const);
        Rectangle hbB = new Rectangle(position.x, position.y+height-height / height_const, width, height / height_const);
        Rectangle hbL = new Rectangle(position.x, position.y, width / width_const, height);
        Rectangle hbR = new Rectangle(position.x+width-width / 10, position.y, width / width_const, height);
        if (hbT.intersects(box) || hbB.intersects(box)) {
            p.y = yDefault;
        }
        if (hbR.intersects(box) || hbL.intersects(box)) {
            p.x = xDefault;
        }
        return p;
    }

    @Override
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(position.x, position.y, width, height);
        final int row_count = 4;
        for (int i = 0; i < height / row_count; i++) {
            graphics.setColor(color.darker());
            graphics.drawLine(position.x + i, position.y + height - i,
                    position.x + width - 1, position.y + height - i);
            graphics.drawLine(position.x + width - 1 - i, position.y + i,
                    position.x + width - 1 - i, position.y+height);
            graphics.setColor(color.brighter());
            graphics.drawLine(position.x, position.y + i,
                    position.x + width - 1 - i, position.y + i);
            graphics.drawLine(position.x + i, position.y + height - i,
                    position.x + i, position.y);
        }
    }

    public final int getWidth() {
        return width;
    }

    public final int getHeight() {
        return height;
    }

    public final Color getColor() {
        return color;
    }

}

