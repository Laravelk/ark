package gameobjects;

import arkanoid.game.GameView;

import java.awt.*;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ball implements GameObject {
    private Point position = new Point(0,0);
    private Point movement = new Point(1,1);
    private float speed = 0.3f;
    private int radius = 10;

    private static Logger logger = Logger.getLogger(Ball.class.getName());
    private GameView gameView = null;
    private int width;
    private int height;


    public Ball(GameView gameView) {
        this.gameView = gameView;
        this.width = gameView.getWidth();
        this.height = gameView.getHeight();
    }
    public Ball() {
        width = 1280;
        height = 720;
    }


    /*
    * @return false if we lost the ball else true
    * */
    public boolean tick(double deltaTime,
                        LinkedList<Block> blocks, int width, int height, Player player) {
        logger.log(Level.INFO, "tick of Ball");
        position.translate((int)(movement.x*(speed*deltaTime)), (int)(movement.y*(speed*deltaTime)));
        if (Math.abs(position.x) >= Math.abs(width / 2)) {
            movement.x = -movement.x;
        }
        if (position.y <= -height / 2) {
            movement.y = -movement.y;
        }
        if (position.y >= height / 2) {
            gameView.ballWasLosted();
        }

        Rectangle hitbox = new Rectangle(position.x - radius, position.y - radius, radius * 2, radius * 2);
        Point pv = player.bounceVector(hitbox);
        movement.x *= pv.x;
        movement.y *= pv.y;

        for (int i = 0; i < blocks.size(); i++) {
            Block b = blocks.get(i);
            pv = b.bounceVector(hitbox);
            movement.x *= pv.x;
            movement.y *= pv.y;
            if (pv.x < 0 || pv.y < 0) {
                if (null != gameView) {
                    gameView.blockBroken();
                }
                blocks.remove(b);
            }
        }

        return true;
    }

    public Point getPosition() {
        return position;
    }

    public Point getMovement() {
        return movement;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(final float speed) {
        this.speed = speed;
    }



    @Override
    public void render(Graphics graphics) {
        graphics.setColor(Color.red);
        final int radius = 10;
        graphics.fillOval(position.x - radius, position.y - radius, radius * 2, radius * 2);
    }
}
