package gameobjects;

import java.awt.*;

public interface GameObject {
    public void render(Graphics graphics);
}
