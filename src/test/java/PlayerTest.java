import gameobjects.Player;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class PlayerTest {
    @Test
    public void PlayerTest() {
        final int CORR_X_1 = 1;
        final int CORR_Y_1 = 1;
        final int gameHeight = 720;
        Player player = new Player(gameHeight);

        Rectangle rectangle1 = new Rectangle(10,20);
        Rectangle rectangle2 = new Rectangle(40,60);
        Point point1 = player.bounceVector(rectangle1);
        Point point2 = player.bounceVector(rectangle2);

       Assert.assertTrue(CORR_X_1 == point1.x && CORR_Y_1 == point1.y);
          Assert.assertTrue(CORR_X_1 == point2.x && CORR_Y_1 == point2.y);
    }
}
