import gameobjects.Ball;
import gameobjects.Block;
import gameobjects.Player;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.util.LinkedList;

public class BallTest {
    @Test
    public void bounce() {
        final int deltaTime = 30;
        final int gameHeight = 720;
        final int gameWidth = 1280;
        final int rowsCount = 6;
        final int columnsCount = 10;
        final Player player = new Player(gameHeight);

        LinkedList<Block> blocks = createBlock(rowsCount, columnsCount, gameWidth, gameHeight);
        blocks.add(new Block());
        Ball ball = new Ball();

        ball.tick(deltaTime, blocks, gameWidth, gameHeight, player);

        Assert.assertTrue(true);
    }

    LinkedList<Block> createBlock(int rows, int columns, int windowWidth, int windowHeight) {
        LinkedList<Block> blocks = new LinkedList<>();
        final int gap = 10;
        final float w_step = (((float) windowWidth - gap) / columns) - gap;
        final float h_step = 30;

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                Block block = new Block(new Point((int) (i * (w_step + gap) + gap) - windowWidth / 2,
                        (int) (j * (h_step + gap) + gap) - windowHeight / 2 + 3 * gap), 70, 30,
                        Color.black);
                blocks.add(block);
            }
        }
        return blocks;
    }
}
