import gameobjects.Block;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class BlockTest {
    @Test
    public void bounceTest() {
        final int CORR_X_1 = -1;
        final int CORR_Y_1 = -1;
        Rectangle rectangle1 = new Rectangle(10,20);
       Rectangle rectangle2 = new Rectangle(40,60);
        Block block = new Block();
        Block block2 = new Block(1280,720, Color.red);
        Point point1 = block.bounceVector(rectangle1);
        Point point2 = block.bounceVector(rectangle2);

        Assert.assertTrue(CORR_X_1 == point1.x && CORR_Y_1 == point1.y);
        Assert.assertTrue(CORR_X_1 == point2.x && CORR_Y_1 == point2.y);

    }
}
